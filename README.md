# fluentZ

Exposes singular and simple REST API endpoint that provides
necessary statefull logic for `REST API` service calls that manage Debezium 
CDC-connector via `Apache Kafka Connect` or likewise platform in gracefull 
and safe manner resulting continous loss-free streams of captured database 
changes in terra- scale.

You can capture fluent loss-free stream of database changes (CDC) 
in near-realtime via fault-tolerant connection to PostgreSQL database 
cluster with Debezium open source connector managed by `fluentZ`.

`fluentZ` is a distributed statefull HA-service (shall be) written in `Go`
relying on distributed `etcd` KV storage shared with any PostgreSQL cluster
manager like `Patroni` or another wich is likely relying on common
distributed storage service or HA-cluster framework.

Service logic eliminates losses in CDC streams due to `failover`, `switchover`
in database HA-cluster on which events postgres nodes instantly changing
their roles and shurely discards current logical replication slots were exposed 
to connector when it comes to failures or whatever-takeover windows.
